const canvas = document.getElementById('canvas'),
      ctx    = canvas.getContext('2d');

      canvas.width = 700
      canvas.height = 500

const btnNext = document.querySelector('.btn-next'),
      btnBack = document.querySelector('.btn-back');
const [...imgMass] = document.querySelectorAll('img'); // масив картинок

let imgCounter = 1;

btnNext.addEventListener('click', nextSlide);
btnBack.addEventListener('click', backSlide);

ctx.drawImage(imgMass[0], 0, 0, 700, 500) // первый слайд

function nextSlide() {

    ctx.beginPath()
    ctx.drawImage(imgMass[imgCounter], 0, 0, 700, 500);

    if (imgCounter == imgMass.length - 1) {
        imgCounter = 0
    } else {
        imgCounter += 1
    }

}

function backSlide() {

    ctx.beginPath()
    ctx.drawImage(imgMass[imgCounter], 0, 0, 700, 500);

    if (imgCounter <= 0) {
        imgCounter = imgMass.length - 1
    } else {
        imgCounter -= 1
    }

}